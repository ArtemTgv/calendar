
window.onhashchange = refreshFunc;

function updateContent(URL) {
	var Content = '';
	var afterFunc = null;
	switch (URL.split('-')[0]) {
		case 'month':
      Content = getMonthView();
			afterFunc = [monthToDayView];
      break;
    case 'day':
      Content = getDayView(URL);
			afterFunc = [dayToMonthView, showAddEvButton, editEv];
      break;
  }
  document.getElementById('mainFr').innerHTML = Content;

	for (var fi in afterFunc) {
		if (typeof(afterFunc[fi]) === 'function') {
			afterFunc[fi]();
		}
	}
}

function refreshFunc() {
	var URLHash = window.location.hash;
	var URLname = URLHash.substr(1);
	if (URLname !== '') {
		updateContent(URLname);
	}
	else {
		goTo('month-');
	}
}

function goTo(section) {
		location.hash = section;
	}


var wkMonth = new CalendarStorage();
refreshFunc();
