
function getMonthView() {
  var cMonth = wkMonth || '';

  var htmlString = '';
  htmlString += '<div id="header"><span class="month" id="'+new Date().getMonth()+'">';
  htmlString += '<span style="color:white">' + String.fromCharCode(9664) + ' </span>';
  htmlString += new Date().getMonthName() + '</span><span id="year">';
  htmlString += new Date().getFullYear() + '</span></div>';
  htmlString += '<table id="monGrid"><thead><th>Mon</th><th>Tue</th><th>Wed</th>';
  htmlString += '<th>Thu</th><th>Fri</th><th>Sat</th><th>Sun</th></thead><tbody>';

  // generate table rows and cells
  var cell = 0;
  var countRow = 7; // daysAWeek
  var row = '';
  var list = cMonth.datesOrder;
  while (cell < list.length) {
    var day = list[cell] || '';
    if (countRow > 0) {
      row += '<td';
      row += ' id="'+day+'"';
      row += ' class="';
      row += cMonth[day].isWeekend ? 'weekEnd' : 'ferial';
      row += (day.split('.')[1] == new Date().getDate()) ? ' today ' : '';
      row += (day.split('.')[0] != new Date().getMonth()) ? ' otherMonth ' : '';
      row += '">';
      row += '<div>' + day.split('.')[1] + '</div>';
      row += '<div class="eventsDiscr">' + (cMonth[day].isHolyday || '') + '</div>';
      if (Object.keys(cMonth[day].EV).length) {
        row += '<div class="uEvent"> Sheduled: '+Object.keys(cMonth[day].EV).length+'</div>'
      }
      row +='</td>';
      cell++;
      countRow --;
    } else {
      htmlString += '<tr>';
      htmlString += row;
      htmlString += '</tr>';
      countRow = 7;
      row = '';
    }
  }
  htmlString += '</tbody></table>';

  return htmlString;
}
