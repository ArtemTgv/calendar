
function monthToDayView() {
  var dayCells = document.getElementsByTagName('td');
  for (var d in dayCells) {
    var elem = dayCells[d];
    elem.style ? elem.style.cursor = 'pointer' : null;
    elem.onclick = function(event) {
      var mdd = event.target.id || event.target.offsetParent.id;
      goTo('day-'+mdd);
    };
  }
}

function dayToMonthView() {
  var elem = document.getElementsByClassName('month')[0];
  elem.style.cursor = 'pointer';
  elem.onmouseover = function(event) {
    event.target.style.textDecoration = 'underline';
  };
  elem.onmouseout = function(event) {
    event.target.style.textDecoration = 'none';
  };
  elem.onmousedown = function(event) {
    event.target.style.textShadow = '-1px -1px 0px rgba(200, 200, 200, 0.9)';
  }
  elem.onclick = function(event) {
    goTo('month-');
    }
  }

function showAddEvButton() {
  var cMonth = document.getElementsByClassName('month')[0].id;
  var cDay = document.getElementsByClassName('dayOfM')[0].id;
  if (new Date().getDate() > cDay && new Date().getMonth() >= cMonth) {
    return;
  }
  var eventCells = document.getElementsByClassName('evBlock');
  for (var e in eventCells) {
    var elem = eventCells[e];
    if (new Date().getDate() == cDay && new Date().getHours() > elem.id) {
      continue;
    }
    elem.onmouseenter = function(event) {
      var tBlock = event.target;
      tBlock.style.cursor = 'pointer';
      tBlock.style.backgroundColor = '#e9e7e7';
      tBlock.onclick = addEvForm;
      tBlock.onmouseleave = function(event) {
        tBlock.style.cursor = 'black';
        tBlock.style.backgroundColor = '#f4f4f4';
        tBlock.onclick = null;
      }
    }
  }
}

function addEvForm(event) {
  if (document.getElementById('addEvDiv') || event.target.type === 'button') {
    return;
  }
  var delButt = '';
  var tBlock = event.target;
  var addDiv = document.createElement('div');
  addDiv.id = 'addEvDiv';
  if (tBlock.className === 'evCover' ||
              tBlock.className === 'uEvent') {
    addDiv.style.left = '3px';
    addDiv.style.top = '-5px';
    tBlock = tBlock.className === 'uEvent' ? tBlock.parentNode : tBlock;
    delButt += '<span class="fBox">';
    delButt += '<input id="delB" type="button" value="Delete"></span>';
  }
  var eaString = '';
  eaString += '<form name="evAddForm" action="javascript:processForm()">'
  eaString += '<span class="fBox"><label>Minutes: </label>';
  eaString += '<select id="eMinutes" name="eMinutes">';
  for (var i=0; i<60; i++) {
    var minutes = i < 10 ? '0'+i : i;
    eaString += '<option value="'+minutes+'">'+minutes+'</option>';
  }
  eaString += '</select></span>';
  eaString += '<span class="fBox"><label>Event: </label>';
  eaString += '<input required maxlength="35" id="eName" name="eName" placeholder="Meet Alice"></span>';
  eaString += '<span class="fBox"><label>Notes: </label>';
  eaString += '<input id="eNotes" name="eNotes" maxlength="90" size="28" placeholder="Don\'t forget flowers!" ></span>';
  eaString += '<span class="fBox"><input id="saveB" type="submit" value="Save"></span>';
  eaString += delButt;
  eaString += '<span class="fBox"><input type="button" value="X" id="closeX"></span>';
  eaString += '</form>';
  addDiv.innerHTML = eaString;
  tBlock.appendChild(addDiv);
  if (tBlock.className !== 'evBlock') {
    var monthE = document.getElementsByClassName('month')[0];
    var dayE = document.getElementsByClassName('dayOfM')[0];
    var date = monthE.id +'.'+ dayE.id;
    var timeE = tBlock.id;
    eTime = document.getElementById('eMinutes');
    eName = document.getElementById('eName');
    eNotes = document.getElementById('eNotes');
    eTime.value = timeE.split(':')[1];
    eName.value = wkMonth[date].EV[timeE][0];
    eNotes.value = wkMonth[date].EV[timeE][1];
    var delB = document.getElementById('delB');
    function deleteEv() {
      wkMonth.delEvent(date, timeE);
      refreshFunc();
    }
    delB.onclick = deleteEv;
  }
  var closeX = document.getElementById('closeX');
  closeX.onclick = refreshFunc;
}

function processForm() {
  var eForm = document.forms['evAddForm'];
  var eMin = eForm['eMinutes'].value;
  var eName = eForm['eName'].value;
  var eNotes = eForm['eNotes'].value;
  if (!eName.length || eName.length > 36) {
    alert('Name of event is requiered!');
    return false;
  }
  if ((eNotes.length < 5 || 120 < eNotes.length) && eNotes.length !== 0) {
    alert('Notes Min 5 and Max 120 symbols!');
    return false;
  }
  var dayElem = document.getElementsByClassName('dayOfM')[0];
  var monthElem = document.getElementsByClassName('month')[0];
  var evDiv = document.getElementById('addEvDiv');
  var date = monthElem.id +'.'+ dayElem.id;
  var timeHo = evDiv.parentNode.id;
  var hour = timeHo;
  if (timeHo.indexOf(':') > 0) {
    wkMonth.delEvent(date, timeHo);
    hour = timeHo.split(':')[0];
  }
  var time = hour + ':' + eMin;
  wkMonth.addEvent(date, time, eName, eNotes);
  refreshFunc();
}

function editEv() {
  var dayEvs = document.getElementsByClassName('evCover');
  for (cEv in dayEvs) {
    dayEvs[cEv].onmouseenter = function(event) {
      event.target.style.backgroundColor = '#6d9cb5';
    }
    dayEvs[cEv].onmouseleave = function(event) {
      event.target.style.backgroundColor = '#9CC1D4';
    }
  }
}
