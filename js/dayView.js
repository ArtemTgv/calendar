
function getDayView(forDay) {
  forDay = forDay.split('-')[1];
  if (!forDay) {
    goTo('month-');
  }
  var dayStore = wkMonth[forDay];

  var curMonth = forDay.split('.')[0];
  var curDate = forDay.split('.')[1];
  var evStorage = dayStore.EV;

  var htmlString = '';
  htmlString += '<div id="header"><span class="month" id="'+curMonth+'">' + String.fromCharCode(9664);
  htmlString += ' ' + new Date().getMonthName(curMonth)+'</span>';
  htmlString += '<span class="dayOfM" id="'+curDate+'"> ' + curDate + ',</span>';
  htmlString += '</span><span id="year">'+new Date().getFullYear()+'</span>';
  if (!Object.keys(evStorage).length) {
    htmlString += '<span id="noEvents">You do not have sheduled events</span>';
  }
  htmlString += '</div>';
  htmlString += '<table id="dayGrid"><tbody>';

  // generate table rows and cells
  var hour = 0;
  while (hour < 24) {
    htmlString += '<tr><td class="time"><div class="itm">' + hour + ':00</div></td>';
    htmlString += '<td class="evBlock" id="' + hour + '">';
    // sort and add sheduled events
    var tArr = [], n=0;
    for (var m in evStorage) {
      if (m.split(':')[0] == hour) {
        tArr[n++] = m.split(':')[1] ;
      }
    }
    tArr = tArr.sort();
    if (tArr.length > 0) {
    for (var e=0; e < tArr.length; e++) {
      var hh = hour + ':' + tArr[e];
      htmlString += '<div class="evCover" id="' + hh + '">';
      htmlString += '<span class="uEvent">' + hh + '</span>';
      htmlString += '<span class="uEvent">' +evStorage[hh][0] + '</span>';
      htmlString += evStorage[hh][1] ? '<span class="uEvent">'+evStorage[hh][1]+'</span>' : '';
      htmlString += '</div><br />';
    }
  }
    htmlString += '</td></tr>';
    hour++;
    }

  htmlString += '</tbody></table>';

  return htmlString;
}
