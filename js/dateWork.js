
Date.prototype.getDaysInMonth = function() {
		return 33 - new Date(this.getFullYear(), this.getMonth(), 33).getDate();
};
Date.prototype.monFirstWeekDay = function() {
	var curDate = new Date;
	return new Date(curDate.getFullYear(), curDate.getMonth(), 1).getDay();
};
Date.prototype.getMonthName = function(ordNum) {
	var ordNum = ordNum || new Date().getMonth();
	var names = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December'
	];
	return names[ordNum];
}
