
function CalendarStorage() {
  this.datesOrder = [];
  //  'Month.Date' : {
  //                   isHolyday : 'holydayName' ||  '',
  //                   isWeekend : 'weekEnd'  ||  '',
  //                   EV: {
  //                         'hour:minutes': ['header', 'discr'],
  //                          ...
  //                       }
  //                  }
  this.startCalendar();
}

CalendarStorage.prototype.startCalendar = function() {
	var storeKey = new Date().getFullYear() + '-' + new Date().getMonth();
	if (storeKey in localStorage) {
		this.getCalfromLS(storeKey);
	} else {
		this.generateDates();
	}
}

CalendarStorage.prototype.generateDates = function() {
  var curDate = new Date();
  var weeks = 6;
  var daysAWeek = 7;
  var holydays = {
    '0.1' : {'isHolyday' : 'New Year\'s Day', 'isWeekend' : true},
    '0.7' : {'isHolyday' : 'Orthodox Christmas Day', 'isWeekend' : true},
    '1.23': {'isHolyday' : 'Defender of the Fatherland Day'},
    '2.8' : {'isHolyday' : 'Women\'s day', 'isWeekend' : true},
    '2.27': {'isHolyday' : 'Catholic Easter'},
    '3.2' : {'isHolyday' : 'Union Day of Belarus and Russia'},
    '4.1' : {'isHolyday' : 'Labour Day', 'isWeekend' : true},
    '4.9' : {'isHolyday' : 'Victory Day', 'isWeekend' : true},
    '4.10': {'isHolyday' : 'Radonitca', 'isWeekend' : true},
    '5.22': {'isHolyday' : 'Remembrance Day of Victims of the Great Patriotic War'},
    '6.3' : {'isHolyday' : 'Independence Day of the Republic of Belarus', 'isWeekend' : true},
    '10.7': {'isHolyday' : 'October Revolution Day', 'isWeekend' : true},
    '11.25': {'isHolyday' : 'Catholic Christmas Day', 'isWeekend' : true},
    '11.31': {'isHolyday' : 'New Year\'s Eve'}
  };
  // how many days of prev month to show
  // if current month started at prevMonDays[i] day of week
  var prevMonDays = [
    6,  // Sun
    7,  // Mon
    1,  // Tue
    2,  // Wen
    3,  // Thu
    4,  // Fri
    5   // Sat
  ]
  var d = prevMonDays[curDate.monFirstWeekDay()];   //  days to currMonth 1st
  var calEnd = (weeks * daysAWeek) - d;   // 42 days in table prev Mon Days
  var i = 0;
  var date = null;
  while (i < calEnd) {
    if (d > 0) {
      d--;
      date = new Date(curDate.getFullYear(), curDate.getMonth(), -d);
    } else {
      i++;
      date = new Date(curDate.getFullYear(), curDate.getMonth(), +i);
    }
    var shortD = date.getMonth() + '.' + date.getDate();

    if (holydays[shortD]) {
      this[shortD] = JSON.parse(JSON.stringify(holydays[shortD]));
    } else {
      this[shortD] = {};
    }
    if (date.getDay() === 6 || date.getDay() === 0) {
      this[shortD].isWeekend = true;
    }
    this[shortD].EV = {};

    this.datesOrder.push(shortD);
  }
};

CalendarStorage.prototype.getMonthName = function(ordNum) {
	var ordNum = ordNum || new Date().getMonth();
	var names = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December'
	];
	return names[ordNum];
};

CalendarStorage.prototype.addEvent = function(date, time, header, discr) {
  this[date].EV[time] = [header, discr];
  this.saveCalToLS();
};
CalendarStorage.prototype.delEvent = function (date, time) {
  delete this[date].EV[time];
  this.saveCalToLS();
};

CalendarStorage.prototype.saveCalToLS = function() {
  if (('localStorage' in window) && (window.localStorage!==null)) {
    var storeKey = new Date().getFullYear() + '-' + new Date().getMonth();
    localStorage[storeKey] = JSON.stringify(this);
  } else {
    alert('Your data can not be saved! Update your browser, please.');
  }
};

CalendarStorage.prototype.getCalfromLS = function(storeKey) {
  var storeKey = storeKey || new Date().getFullYear() +'-'+ new Date().getMonth();
	var storedMonth = JSON.parse(localStorage[storeKey]);
  for (var d in storedMonth) {
    this[d] = storedMonth[d];
  }
};
